# ScrumShuffle

ScrumShuffle is an easy to use cli tool that allows to shuffles a list of participants across multiple rounds.

## How to use

```
Usage: ScrumShuffle.exe [OPTIONS]

Options:
  -h,--help                   Print this help message and exit
  -s,--shuffle TEXT=.\shuffle.json
                              Path to an JSON file that describe the Suffle.
```

Shuffle file example.  

The rounds are executed in their defined order, but the order of the participants within a round is randomized and each participant is introduced using randomly selected sentence.

Pressing `enter` simply proceeds to the next participant. Pressing `y` does the same, and marks the current participant as requiring further discussion during the final "extra" round.

```json
{
    "introduction": "Quest Assignements",
    "extra": "Sidequests",
    "conclusion": "Go forth!",
    "rounds": [
        {
            "sentences": [
                "The honorable {0}",
                "Introducing {0}",
                "The valiant {0}"
            ],
            "participants": [
                "Lancelot",
                "Gawain",
                "Geraint",
                "Percival",
                "Bors the Younger",
                "Lamorak"
            ]
        },
        {
            "sentences": [
                "The honorable {0}",
                "Introducing {0}",
                "The valiant {0}"
            ],
            "participants": [
                "Gareth",
                "Bedivere",
                "Gaheris",
                "Galahad",
                "Tristan",
                "Palamedes"
            ]
        }
    ]
}
```

## How to build

This project uses CMake with C++17. Some CMake presets are available in `CMakePresets.json` (see [cmake-presets](https://cmake.org/cmake/help/latest/manual/cmake-presets.7.html)). With these presets, dependencies are managed automatically with `vcpkg`.

When configuring with Visual Studio Code, `code` needs to be run from a Visual Studio Developer Command Prompt in order to properly setup the environment for CMake.
